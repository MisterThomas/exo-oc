package com.oc.game;

public class tableau {
    public static void main(String[] args) {
        String[] tabStr = {"toto", "titi", "tata"};
        int[] tabInt = {1, 2, 3, 4};
        String[][] tabStr2 = {{"1", "2", "3", "4"}, {"toto", "titi", "tata"}};

        //La méthode avec un tableau de String sera invoquée
        parcourirTableau(tabStr);
//La méthode avec un tableau d'int sera invoquée
        parcourirTableau(tabInt);
//La méthode avec un tableau de String à deux dimensions sera invoquée
        parcourirTableau(tabStr2);
    }

    private static void parcourirTableau(String[] tabStr) {
    }

    private static void parcourirTableau(int[] i) {
    }

    static void parcourirTableau(String[][] tab) {
        for (String tab2[] : tab) {
            for (String str : tab2)
                System.out.println(str);
        }
    }

    static void tabBrowse(int[] tab) {
        for (int str : tab)
            System.out.println(str);
    }

    static String strJobs(String[] tab) {
        System.out.println("Méthode to string!\\n----------");
        String retour = "";

        for (String str : tab)
            retour += str + "\n";

        return retour;
    }

}
