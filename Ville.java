package com.oc.game;

public class Ville {
    public static int nbreInstances = 0;
    private static int nbreInstancesBis = 0;

    public static void main(String[] args) {
        Ville v = new Ville();
        Ville v1 = new Ville("Marseille", 123456, "France");
        Ville v2 = new Ville("Rio", 321654, "Brésil");

        System.out.println("\n\n"+v1.decrisToi());
        System.out.println(v.decrisToi());
        System.out.println(v2.decrisToi()+"\n\n");
        System.out.println(v1.comparer(v2));

/*
  Nous allons interchanger les Villes v1 et v2
  tout ça par l'intermédiaire d'un autre objet Ville.
*/
        Ville temp = new Ville();
        temp = v1;
        v1 = v2;
        v2 = temp;

        System.out.println(" v1 = "+v1.getNom()+" ville de  "+v1.getNombreHabitants()+ " habitants se situant en "+v1.getNomPays());
        System.out.println(" v2 = "+v2.getNom()+" ville de  "+v2.getNombreHabitants()+ " habitants se situant en "+v2.getNomPays()+"\n\n");

/*
  Nous allons maintenant interchanger leurs noms
  cette fois par le biais de leurs mutateurs.
*/
        v1.setNom("Hong Kong");
        v2.setNom("Djibouti");

        System.out.println(" v1 = "+v1.getNom()+" ville de  "+v1.getNombreHabitants()+ " habitants se situant en "+v1.getNomPays());
        System.out.println(" v2 = "+v2.getNom()+" ville de  "+v2.getNombreHabitants()+ " habitants se situant en "+v2.getNomPays()+"\n\n");

        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.nbreInstances);
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.getNombreInstancesBis());

        Ville v3 = new Ville("Marseille", 1236, "France");
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.nbreInstances);
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.getNombreInstancesBis());

        Ville v4 = new Ville("Rio", 321654, "Brésil");
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.nbreInstances);
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.getNombreInstancesBis());
    }



    public static int getNombreInstancesBis()
    {
        return nbreInstancesBis;
    }
    //Le reste du code est le même qu'avant

    private String nomVille;
    private String nomPays;
    private int nbreHabitants;
    private char categorie;

    public Ville(){
        System.out.println("Création d'une ville !");
        nomVille = "Inconnu";
        nomPays = "Inconnu";
        nbreHabitants = 0;
        nbreInstances++;
        nbreInstancesBis++;
        this.setCategorie();
    }

    public Ville(String pNom, int pNbre, String pPays)
    {
        System.out.println("Création d'une ville avec des paramètres !");
        nomVille = pNom;
        nomPays = pPays;
        nbreHabitants = pNbre;
        nbreInstances++;
        nbreInstancesBis++;
        this.setCategorie();
    }

    //Retourne le nom de la ville
    public String getNom()  {
        return nomVille;
    }

    //Retourne le nom du pays
    public String getNomPays()
    {
        return nomPays;
    }

    // Retourne le nombre d'habitants
    public int getNombreHabitants()
    {
        return nbreHabitants;
    }

    //Retourne la catégorie de la ville
    public char getCategorie()
    {
        return categorie;
    }

    //Définit le nom de la ville
    public void setNom(String pNom)
    {
        nomVille = pNom;
    }

    //Définit le nom du pays
    public void setNomPays(String pPays)
    {
        nomPays = pPays;
    }

    //Définit le nombre d'habitants
    public void setNombreHabitants(int nbre)
    {
        nbreHabitants = nbre;
        this.setCategorie();
    }

    //Définit la catégorie de la ville
    private void setCategorie() {

        int bornesSuperieures[] = {0, 1000, 10000, 100000, 500000, 1000000, 5000000, 10000000};
        char categories[] = {'?', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};

        int i = 0;
        while (i < bornesSuperieures.length && this.nbreHabitants > bornesSuperieures[i])
            i++;

        this.categorie = categories[i];
    }

    //Retourne la description de la ville
    public String decrisToi(){
        return "\t"+this.nomVille+" est une ville de "+this.nomPays+ ", elle comporte : "+this.nbreHabitants+" habitant(s) => elle est donc de catégorie : "+this.categorie;
    }

    //Retourne une chaîne de caractères selon le résultat de la comparaison
    public String comparer(Ville v1){
        String str = new String();

        if (v1.getNombreHabitants() > this.nbreHabitants)
            str = v1.getNom()+" est une ville plus peuplée que "+this.nomVille;

        else
            str = this.nomVille+" est une ville plus peuplée que "+v1.getNom();

        return str;
    }
}
